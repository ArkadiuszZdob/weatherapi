﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherAPI.Interfaces
{
    public interface IAdaptWrapper
    {
        string TakeAnswer();
    }
}
