﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeatherAPI.Models;


namespace WeatherAPI.Interfaces
{
    public interface IManageHistory
    {
        string AddRequest(APIHistory model);

        List<APIHistory> GetHistory();
    }
}
