﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WeatherAPI.Models;
using System.Configuration;
using System.Net;
using System.Text;
using System.IO;
using Newtonsoft.Json;
using WeatherAPI.Interfaces;
using WeatherAPI.Patterns;

namespace WeatherAPI.Concrete
{
    public class APIManagerPolish : IManager
    {
        private string _apiKey;
        private LANGUAGE_TYPE _language;
        private APIPolishInfo polishInfo;

        public APIManagerPolish(LANGUAGE_TYPE langParam)
        {
            _apiKey = @ConfigurationManager.AppSettings["WeatherKey"];
            _language = langParam;
            polishInfo = new APIPolishInfo();
        }


        /// <summary>
        /// Zwraca przetworzony obiekt
        /// </summary>
        /// <param name="location"></param>
        /// <returns></returns>
        APIPolishInfo IManager.GetContent<APIPolishInfo>(string location)
        {
            Rootobject model;
            APIPolishInfo apiInfo;
            try
            {
                HttpWebRequest request = _CreateRequest(location);
                String content = _GetResponse(request);
                model = DeserializeWeather(content);
                SettingsSingleton._currentMeasurement = model;

                apiInfo = (APIPolishInfo)Activator.CreateInstance(typeof(APIPolishInfo), new object[] { model.current_observation.feelslike_c,
                model.current_observation.feelslike_f,model.current_observation.observation_time_rfc822,model.current_observation.relative_humidity,
                model.current_observation.visibility_km,model.current_observation.visibility_mi,model.current_observation.wind_degrees,
                model.current_observation.wind_dir,model.current_observation.wind_kph,model.current_observation.wind_mph,location});

            }
            catch (Exception)
            {
                return default(APIPolishInfo);
            }

            return apiInfo;
        }

        /// <summary>
        /// Deserializacja
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        private Rootobject DeserializeWeather(String content)
        {
            try
            {
                return JsonConvert.DeserializeObject<Rootobject>(content);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Pobieranie wyników z adresu
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private String _GetResponse(HttpWebRequest request)
        {
            string builder;
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());
                builder = reader.ReadToEnd();
            }
            return builder;
        }

        /// <summary>
        /// Tworzenie adresu url
        /// </summary>
        /// <param name="location"></param>
        /// <returns></returns>
        private HttpWebRequest _CreateRequest(string location)
        {
            HttpWebRequest request = WebRequest.Create(String.Format("http://api.wunderground.com/api/{0}/conditions/q/CA/{1}.json", _apiKey,location)) as HttpWebRequest;
            request.Method = "GET";

            return request;
        }

    }
}