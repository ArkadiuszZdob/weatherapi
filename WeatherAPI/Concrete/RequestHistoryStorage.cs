﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WeatherAPI.Interfaces;
using WeatherAPI.Models;
using WeatherAPI.Patterns;

namespace WeatherAPI.Concrete
{
    public class RequestHistoryStorage : IManageHistory
    {
        private List<APIHistory> requestsHistory;

        public RequestHistoryStorage()
        {
            requestsHistory = new List<APIHistory>();
        }

        string IManageHistory.AddRequest(APIHistory model)
        {
            if (model != null)
            {
                SettingsSingleton.requestsHistory.Add(model);
                return "Pomyślnie dodano do historii!";
            }
            else
            {
                return "Nie udało się dodać do historii!";
            }
            
        }

        List<APIHistory> IManageHistory.GetHistory()
        {
            return SettingsSingleton.requestsHistory;
        }
    }
}