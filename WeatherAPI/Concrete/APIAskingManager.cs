﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WeatherAPI.Models;
using System.Text;

namespace WeatherAPI.Concrete
{
    public class APIAskingManager
    {
        private decimal? _temperature = null;
        private decimal? _humidity = null;
        private LANGUAGE_TYPE _language;

        public APIAskingManager(string tempParam,string humidParam,LANGUAGE_TYPE langParam)
        {
            _temperature = decimal.Parse(tempParam.Replace('.',','));
            _humidity = decimal.Parse(humidParam.Remove(humidParam.Length-1,1));
            _language = langParam;
        }

        public string GetAnswer()
        {
            string result;

            if (_language == LANGUAGE_TYPE.POLISH)
            {
                result = CheckWeather_Pol();
            }
            else
            {
                result = CheckWeather_Eng();
            }

            return result;
        }

        private string CheckWeather_Pol()
        {
            StringBuilder result = new StringBuilder();

            if (_temperature < 0m)
            {
                result.AppendLine("Jest bardzo zimno - zostań w domu. ");
            }
            else if (_temperature >= 0m && _temperature <= 20m)
            {
                result.AppendLine("Jest chłodno - ubierz się ciepło. ");
            }
            else if (_temperature > 20m)
            {
                result.AppendLine("Jest ciepło - możesz śmiało wyjść na dwór. ");
            }

            if (_humidity < 40m)
            {
                result.AppendLine("Bardzo suche powietrze, pamiętaj o tym ! ");
            }
            else if (_humidity >= 40m && _humidity <= 60m)
            {
                result.AppendLine("Idealna wilgotność powietrza, bez obaw. ");
            }
            else if (_humidity > 60m)
            {
                result.AppendLine("Wysoka wilgotność, wysokie prawdopodobieństwo opadów atmosferycznych. ");
            }

            return result.ToString();
        }

        private string CheckWeather_Eng()
        {
            StringBuilder result = new StringBuilder();

            if (_temperature < 0m)
            {
                result.AppendLine("Is so cold - stay at home. ");
            }
            else if (_temperature >= 0m && _temperature <= 20m)
            {
                result.AppendLine("Is cold - get some warm clothes. ");
            }
            else if (_temperature > 20m)
            {
                result.AppendLine("Is hot - go bold outside. ");
            }

            if (_humidity < 40m)
            {
                result.AppendLine("Very dry air, don't forget about it ! ");
            }
            else if (_humidity >= 40m && _humidity <= 60m)
            {
                result.AppendLine("Ideal air humidity, don't be afraid. ");
            }
            else if (_humidity > 60m)
            {
                result.AppendLine("High air humidity, probably rain. ");
            }

            return result.ToString();
        }

    }
}