﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WeatherAPI.Interfaces;
using WeatherAPI.Concrete;
using WeatherAPI.Models;

namespace WeatherAPI.Patterns.Adapter
{
    public class AskingWrapper : APIAskingManager,IAdaptWrapper
    {
        public AskingWrapper(string tempParam,string humParam, LANGUAGE_TYPE langParam):base(tempParam,humParam,langParam)
        {
        }
        string IAdaptWrapper.TakeAnswer()
        {
            return GetAnswer();
        }
    }
}