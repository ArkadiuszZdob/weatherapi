﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WeatherAPI.Concrete;
using WeatherAPI.Interfaces;
using WeatherAPI.Models;

namespace WeatherAPI.Patterns.Factory
{
    /// <summary>
    /// Tworzy obiekt dla WeatherAPI
    /// </summary>
    public class FactoryApi
    {
        public FactoryApi()
        { }

        public IManager CreateAPIManager(LANGUAGE_TYPE l_type)
        {
            if (l_type == LANGUAGE_TYPE.POLISH)
            {
                return new APIManagerPolish(l_type);
            }
            else if (l_type == LANGUAGE_TYPE.ENGLISH)
            {
                return new APIManagerEnglish(l_type);
            }
            return null;
        }
    }
}