﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WeatherAPI.Models;

namespace WeatherAPI.Patterns
{
    /// <summary>
    /// Singleton przechowujący dane aplikacji
    /// </summary>
    public sealed class SettingsSingleton
    {
        private static SettingsSingleton _instance = null;
        private static readonly object _lock = new object();
        private static Dictionary<string, string> _versionDirectory;

        public static SettingsSingleton Instance
        {
            get
            {
                lock (_lock)
                {
                    if (_instance == null)
                    {
                        _instance = new SettingsSingleton();
                    }
                    return _instance;
                }
            }
        }

        public static KeyValuePair<string,string> Version
        {
            get { return _versionDirectory.Last(); }
        }

        public static Rootobject _currentMeasurement { get; set; }
        public static LANGUAGE_TYPE _currentLanguage { get; set; }
        public static List<APIHistory> requestsHistory { get; set; } = new List<APIHistory>();

        private SettingsSingleton()
        {
            _versionDirectory = new Dictionary<string, string>()
            {
                {"1.0","Wersja inicjacyjna."}
            };
        }
    }
}