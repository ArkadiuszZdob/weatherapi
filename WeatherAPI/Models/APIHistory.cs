﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeatherAPI.Models
{
    public class APIHistory
    {
        public string CityName { get; set; }
        public string Temperature { get; set; }
        public string Description { get; set; }
    }
}