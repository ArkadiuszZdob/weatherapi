﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeatherAPI.Models
{
    public class APIEnglishInfo
    {
        public APIEnglishInfo()
        { }
        public APIEnglishInfo(string temp_c, string temp_f, string data_bad, string wilgot, string wid_km, string wid_mil, int kat_w, string kierunek_w,
            float wiatr_k, float wiatr_m, string lok)
        {
            temperature_c = String.Format("Temperature (Celsius): {0}*C", temp_c.Replace('.', ','));
            temperature_f = String.Format("Temperature (Kelvin): {0}*K", temp_f.Replace('.', ','));
            date_search = String.Format("Date search: {0}", data_bad.Replace('.', ','));
            humidity = String.Format("Humidity: {0}", wilgot.Replace('.', ','));
            visibility_km = String.Format("Visibility: {0}km", wid_km.Replace('.', ','));
            visibility_mil = String.Format("Visibility: {0}mil", wid_mil.Replace('.', ','));
            wind_angle = String.Format("Wind angle: {0}*", kat_w);
            wind_direction = String.Format("Wind direction: {0}", kierunek_w);
            wind_kph = String.Format("Wind speed: {0}kph", wiatr_k);
            wind_mph = String.Format("Wind speed: {0}mph", wiatr_m);
            location = String.Format("Location: {0}", lok);

        }
        public string temperature_c { get; set; }
        public string temperature_f { get; set; }
        public string date_search { get; set; }
        public string humidity { get; set; }
        public string visibility_km { get; set; }
        public string visibility_mil { get; set; }
        public string wind_angle { get; set; }
        public string wind_direction { get; set; }
        public string wind_kph { get; set; }
        public string wind_mph { get; set; }
        public string location { get; set; }
    }
}