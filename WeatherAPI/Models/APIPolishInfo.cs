﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeatherAPI.Models
{
    public class APIPolishInfo
    {
        public APIPolishInfo()
        { }
        public APIPolishInfo(string temp_c,string temp_f,string data_bad,string wilgot,string wid_km,string wid_mil,int kat_w,string kierunek_w,
            float wiatr_k, float wiatr_m, string lok)
        {
            temperatura_c = String.Format("Temperatura (Celsjusza): {0}*C",temp_c.Replace('.',','));
            temperatura_f = String.Format("Temperatura (Kelwina): {0}*K",temp_f.Replace('.', ','));
            data_badania = String.Format("Data pomiaru: {0}",data_bad.Replace('.', ','));
            wilgotnosc = String.Format("Wilgotność powietrza: {0}",wilgot.Replace('.', ','));
            widocznosc_km = String.Format("Widoczność: {0}km",wid_km.Replace('.', ','));
            widocznosc_mil = String.Format("Widoczność: {0}mil",wid_mil.Replace('.', ','));
            kat_wiatru = String.Format("Kąt wiatru: {0}*",kat_w.ToString());
            kierunek_wiatru = String.Format("Kierunek wiatru: {0}",kierunek_w);
            wiatr_kph = String.Format("Prędkość wiatru: {0}kph",wiatr_k);
            wiatr_mph = String.Format("Prędkość wiatru: {0}mph",wiatr_m);
            lokalizacja = String.Format("Lokalizacja: {0}",lok);

        }
        public string temperatura_c { get; set; }
        public string temperatura_f { get; set; }
        public string data_badania { get; set; }
        public string wilgotnosc { get; set; }
        public string widocznosc_km { get; set; }
        public string widocznosc_mil { get; set; }
        public string kat_wiatru { get; set; }
        public string kierunek_wiatru { get; set; }
        public string wiatr_kph { get; set; }
        public string wiatr_mph { get; set; }
        public string lokalizacja {get;set;}
    }
}