﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WeatherAPI.Interfaces;
using WeatherAPI.Concrete;
using WeatherAPI.Patterns.Factory;
using WeatherAPI.Patterns.Adapter;
using WeatherAPI.Models;
using WeatherAPI.Patterns;

namespace WeatherAPI.Controllers
{
    public class HomeController : Controller
    {
        private IManageHistory _history;
        private FactoryApi _factoryAPI;

        public HomeController(IManageHistory historyParam)
        {
            _history = historyParam;
            _factoryAPI = new FactoryApi();
            
        }

        public ActionResult Index()
        {
            return View();
        }

        

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult LanguageHandling(string language)
        {
            if (language == "0")
            {
                SettingsSingleton._currentLanguage = LANGUAGE_TYPE.POLISH;
                return RedirectToAction("PolishSearcher");
            }
            else
            {
                SettingsSingleton._currentLanguage = LANGUAGE_TYPE.ENGLISH;
                return RedirectToAction("EnglishSearcher");
            }
        }

        public ActionResult PolishSearcher()
        {
            return View();
        }

        public ActionResult EnglishSearcher()
        {
            return View();
        }

        [HttpPost, ValidateAntiForgeryToken]
        public PartialViewResult CheckWeatcher(string location)
        {
            try
            {
                dynamic result;
                IManager APIClient = _factoryAPI.CreateAPIManager(SettingsSingleton._currentLanguage);
                if (SettingsSingleton._currentLanguage == LANGUAGE_TYPE.POLISH)
                {
                    result = APIClient.GetContent<APIPolishInfo>(location);
                    _history.AddRequest(new APIHistory() { CityName = location, Description = "---", Temperature = (result as APIPolishInfo).temperatura_c });
                }
                else
                {
                    result = APIClient.GetContent<APIEnglishInfo>(location);
                    _history.AddRequest(new APIHistory() { CityName = location, Description = "---", Temperature = (result as APIEnglishInfo).temperature_c });
                }

                return PartialView(result);
            }
            catch (Exception)
            {
                return PartialView("Error");
            }
        }

        public PartialViewResult CheckAssistent()
        {
            try
            {
                AskingWrapper assistWrapper = new AskingWrapper(SettingsSingleton._currentMeasurement.current_observation.feelslike_c, SettingsSingleton._currentMeasurement.current_observation.relative_humidity,
                    SettingsSingleton._currentLanguage);

                string result = assistWrapper.GetAnswer();

                return PartialView("CheckAssistent",result);
            }
            catch (Exception)
            {
                return PartialView("Error");
            }
        }

        public ActionResult ShowHistory()
        {
            return View(_history.GetHistory());
        }
    }
}